const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name')
const spanFullName = document.querySelector('#span-full-name');
const spanGreetings = document.querySelector('.span-greetings');

// Alternatives for document.querySelector()
/*
	document.getElementById('txt-firstName');
	document.getElementByClassName('txt-inputs')
	document.getElementByTagName('input');
*/

// Event listener listens to a specific event 
// 'keyup' event indicates which is key is pressed
// (event) parameter is an event handler property
txtFirstName.addEventListener('keyup', (event)=>{
	// showing in the browser the value from an event of inputting a value in a input box
	// the value was showed to a specific tag with an id value of = 'spanFullName'

	// .innerHTML here is used to display text to browser using a HTML tag
	spanFullName.innerHTML = txtFirstName.value;
})

txtFirstName.addEventListener('keyup', (event)=>{
	// showing in the console the html tag, and the input value where the event has held
	console.log(event.target);// shows which html element or tag where the event has happened
	console.log(event.target.value); // shows the value of html element or tag, in this case, the value came from the user input
})

// Additional Example & Mini Activity
// const greetings = () => {
// 	let firstName = txtFirstName.value;
// 	let greeting = 'Hi, welcome! ';
// 	// spanGreetings.innerHTML = greeting + firstName ;
// 	// Hi, welcome firstName
// }
// txtFirstName.addEventListener('keyup', greetings);



const fullname = () => {
    let firstName = txtFirstName.value;
    let lastname = txtLastName.value

    spanFullName.innerHTML = firstName + ' ' + lastname


}

txtFirstName.addEventListener('keyup', fullname);
txtLastName.addEventListener('keyup', fullname)

